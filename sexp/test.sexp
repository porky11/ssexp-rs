(add 1 (mul 2 3)) ;complex math
(add 1 2 3 4 5) ;multiple arguments
(add) ;few arguments
-1 ;no function call
1+2+3 ;infix math
1 + 2*3 ;infix always left associative

