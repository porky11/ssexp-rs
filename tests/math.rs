use ssexp::{
    parse,
    parsers::{DelimitedListParser, InfixKind},
    MacroMap,
    Token::{self, List, Symbol},
};

use num_traits::{One, Zero};

use std::{fmt::Debug, str::FromStr};

fn sum<T: Zero + Copy>(nums: &[T]) -> T {
    nums.iter().fold(T::zero(), |x, y| x + *y)
}

fn prod<T: One + Copy>(nums: &[T]) -> T {
    nums.iter().fold(T::one(), |x, y| x * *y)
}

fn to<T: FromStr + Zero + One + Copy>(tokens: &[Token]) -> Vec<T>
where
    <T as FromStr>::Err: Debug,
{
    tokens
        .iter()
        .map(|token| match token {
            Symbol(string) => string
                .parse()
                .unwrap_or_else(|_| panic!("Parsing number from {string:?} failed")),

            List(toks) => {
                let op = toks[0]
                    .symbol_ref()
                    .unwrap_or_else(|| panic!("First list element has to be a symbol"));

                let nums = &to(&toks[1..])[..];
                match op {
                    "add" => sum(nums),
                    "mul" => prod(nums),
                    _ => panic!("Operation `{op}` not supported"),
                }
            }
        })
        .collect()
}

#[test]
fn math_test() -> Result<(), std::io::Error> {
    use std::io::Read;

    use std::fs::File;

    let mut file = File::open("sexp/test.sexp")?;

    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let map = MacroMap::new()
        .with_lists('(', ')')
        .with_comments(';')
        .with_separating_whitespaces()
        .with_infix('+', InfixKind::Prefix("add".into()), false)
        .with_infix('*', InfixKind::Prefix("mul".into()), false);

    let sexps = parse(contents.chars(), DelimitedListParser(')'), map);

    let nums = to::<i32>(&sexps[..]);
    assert_eq!(nums[0], 7);
    assert_eq!(nums[1], 15);
    assert_eq!(nums[2], 0);
    assert_eq!(nums[3], -1);
    assert_eq!(nums[4], 6);
    assert_eq!(nums[5], 9);

    Ok(())
}
